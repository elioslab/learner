
package wondertech.table;

import java.io.Serializable;
import java.util.Objects;

public class TableElement implements Serializable{
    private final Double[] status;
    private final Integer action_index; 
    private Double value;
    private final Double epsilon = 0.01;
    private Integer visits = 0;

    public TableElement(Double[] status, Integer action_index) {
        this.status = new Double[status.length];
        for (Integer i = 0; i < status.length; i++)
            this.status[i] = status[i];
        this.action_index = action_index;
        this.value = 0.0;
    }

    public Double[] getStatus() {
        return status;
    }

    public Integer getAction_index() {
        return action_index;
    }

    public Double getValue(){
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
    
    public Integer getVisits() {
        return visits;
    }
    
    public void increaseVisits(){
        visits++;
    }
    
    public boolean are_you(Double[] status, Integer action_index){
        if(!Objects.equals(this.action_index, action_index))
            return false;
        
        Double[] my_values = this.status;
        Double[] other_values = status;
        
        for(Integer i=0; i<my_values.length; i++){
            if(my_values[i] - other_values[i] > this.epsilon)
                return false;
        }
        
        return true;
    }
}
