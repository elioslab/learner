package wondertech.qfunctions;

import java.util.ArrayList;
import java.util.List;
import wondertech.network.Network;

public class QFunction_MLP extends QFunction {
    private final List<Network> networks = new ArrayList<>();

    public QFunction_MLP(Integer actions_size, Integer features_size){
        super(actions_size, features_size);
        for(int i=0; i<actions_size; i++)
            networks.add(new Network(features_size, features_size*12));
    }
 
    public Double get(Double[] status, Integer action) {
       return networks.get(action).evaluate(status);
    }
    
    public void update(Double[] status, Integer action, Double value) {
        networks.get(action).train(status, value);        
    }

    public final void load() { 
        for(int i=0; i<networks.size(); i++)
            networks.get(i).load(i);
    }
    
    public void save() { 
        for(int i=0; i<networks.size(); i++)
            networks.get(i).save(i);
    }
}

