package wondertech.qfunctions;

public abstract class QFunction {
    private final Integer actions_size;
    private final Integer features_size;
    
    static public QFunction create(QFunctionType type, int actions_size, int features_size) {
        switch(type) {
            case table:
                return new QFunction_Table(actions_size, features_size);
            case network:
                return new QFunction_MLP(actions_size, features_size);
            default:
                return null;
        }
    }

    public QFunction(Integer actions_size, Integer features_size) {
        this.actions_size = actions_size;
        this.features_size = features_size;
    }
    
    public Integer getActionSize() {return this.actions_size; }
    public Integer getFeatureSize() { return this.features_size; }
    
    abstract public Double get(Double[] status, Integer action); 
    abstract public void update(Double[] status, Integer action, Double value);
    abstract public void load();
    abstract public void save();
    
    public Double max(Double[] status) {
        Double max = get(status, 0);
        for(Integer i=1; i<actions_size; i++) {
            Double temp = get(status, i);
            if(temp > max)
                max = temp;
        } 
        return max;
    }
    
    public Integer best(Double[] status) {
        Integer best = 0;
        Double max = get(status, best);
        for(Integer i=1; i<actions_size; i++) {
            Double temp = get(status, i);
            if(temp > max) {
                max = temp;
                best = i;
            }
        } 
        return best;
    }
    
    public Double min(Double[] status) {
        Double min = get(status, 0);
        for(Integer i=1; i<actions_size; i++) {
            Double temp = get(status, i);
            if(temp < min)
                min = temp;
        } 
        return min;
    }
}

