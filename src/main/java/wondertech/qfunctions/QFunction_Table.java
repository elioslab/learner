package wondertech.qfunctions;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import wondertech.table.TableElement;

public class QFunction_Table extends QFunction {
    private final Double rangeMaxValue = 3.0;
    private final Double rangeMinValue = 0.0;
    
    private CircularFifoQueue<TableElement> table = new CircularFifoQueue<>(100000);
            
    public QFunction_Table(Integer action_size, Integer features_size) {
        super(action_size, features_size);
    }
    
    public Double get(Double[] status, Integer action_index) {
        TableElement element = find(status, action_index);
        element.increaseVisits();
        return element.getValue();
    }
    
    public void update(Double[] status, Integer action, Double value) {
        TableElement element = find(status, action);
        element.setValue(value);
        if(value > rangeMaxValue || value < rangeMinValue)
            adjust(status);
    }
    
    public final void load() { 
        InputStream file = null;
        try {
           if (Files.exists(Paths.get("marioai-table.ser"))) {
                file = new FileInputStream("marioai-table.ser");
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput input_file = new ObjectInputStream (buffer);
                table = (CircularFifoQueue<TableElement>)input_file.readObject();
           }
        } 
        catch (Exception ex) {
            Logger.getLogger(QFunction_Table.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                if (Files.exists(Paths.get("marioai-table.ser")))
                    file.close();
            } 
            catch (IOException ex) {
                Logger.getLogger(QFunction_Table.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void save() { 
        try {
            OutputStream file = new FileOutputStream("marioai-table.ser");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);
            output.writeObject(this.table);
            output.close();
        } 
        catch (IOException ex) {
            Logger.getLogger(QFunction_Table.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void set(Double[] status, Integer action_index, Double value) {
        TableElement element = find(status, action_index);
        element.setValue(value);
    }
    
    private TableElement find(Double[] status, Integer action_index) {
        for(int i=0; i<table.size(); i++){
            TableElement tmp = table.get(i);
            if(tmp.are_you(status, action_index))
                return tmp;
        }
        
        TableElement element = new TableElement(status, action_index);
        table.add(element);
        return element;
    }
    
    private void adjust(Double[] status) {
        Double max = max(status);
        Double min = min(status);
        
        Double oldRange = (max - min);
        Double newRange = (rangeMaxValue - rangeMinValue);
        for(Integer i=0; i<getActionSize(); i++) {
            if (oldRange == 0) {
                set(status, i, rangeMinValue);
            }
            else {
                Double adjusted_value = (((get(status, i) - min) * newRange) / oldRange) + rangeMinValue;
                set(status, i, adjusted_value);
            }
        }    
    }
}

