package wondertech.learner;

public class Interaction {
    private final Status before;
    private final Integer action_index;  
    private final Status after;
    private final Double reward;

    public Interaction(Status before, Integer action_index, Status after, Double reward) {
        this.before = before;
        this.action_index = action_index;
        this.after = after;
        this.reward = reward;
    }

    public Status getBefore() { return before; }
    public Integer getActionIndex() { return action_index; }
    public Status getAfter() { return after; }
    public Double getReward() { return reward; }
}
