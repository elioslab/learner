package wondertech.learner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Status implements Serializable {
    private final List<Double> values = new ArrayList<>();
    
    public Status(Double[][] features, Integer width, Integer height ) {
        map(features, width, height);
    }
    
    public Status(byte[][] features, Integer width, Integer height ) {
        map(features, width, height);
    }
    
    public Status(Double[] features) {
        for (Integer i = 0; i < features.length; i++)
            values.add(features[i]);
    }
    
    public Status(int[] features) {
        for (Integer i = 0; i < features.length; i++)
            values.add((double)features[i]);
    }
    
    public Status(int[] features, int other_feature) {
        for (Integer i = 0; i < features.length; i++)
            values.add((double)features[i]);
        values.add((double)other_feature);
    }
    
    public Status(int[] features, int[] other_features) {
        for (Integer i = 0; i < features.length; i++)
            values.add((double)features[i]);
        for (Integer i = 0; i < other_features.length; i++)
            values.add((double)other_features[i]);
    }
    
    public Status(Status status) {
        for (Integer i = 0; i < status.getValues().length; i++)
            values.add(status.get(i));
    }
    
    public Double get(Integer index) {
        return values.get(index);
    }
    
    public Double[] getValues() { 
        Double[] output = new Double[values.size()];
        return values.toArray(output); 
    }
    
    public void add(Double[] others) {
        for (Integer i = 0; i < others.length; i++)
            values.add(others[i]);
    }
    
    public void add(int[] others) {
        for (Integer i = 0; i < others.length; i++)
            values.add((double)others[i]);
    }
    
    private void map(Double[][] features, Integer width, Integer height) {
        for(int i=-1; i<width-1; i++) 
            for(int j=-1; j<height-1; j++) 
                values.add(probe(i,j, features));
    }
    
    private void map(byte[][] features, Integer width, Integer height) {
        for(int i=-1; i<width-1; i++) 
            for(int j=-1; j<height-1; j++) 
                values.add(probe(i,j, features));
    }
    
    private Double probe(int x, int y, Double[][] features) {
        int realX = x + 11;
        int realY = y + 11;
        return features[realX][realY];
    }
    
    private Double probe(int x, int y, byte[][] features) {
        int realX = x + 11;
        int realY = y + 11;
        return (double)features[realX][realY];
    }
}
