package wondertech.learner;

import wondertech.qfunctions.QFunctionType;
import wondertech.qfunctions.QFunction;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.commons.collections4.queue.CircularFifoQueue;

public class Learner {
    private QFunctionType type;
    private Double alfa;
    private final Double gamma;
    private Double kappa;
    private final Double delta_kappa;
    private final Integer memory_size;
    private final Integer replay_size;
    
    private final QFunction qfunction;
    
    private CircularFifoQueue<Interaction> memory = null;
    private final Random random = new Random();
    
    private Status status = null;
    private Integer action = -1;
    private Double reward = 0.0;
    
    public Learner(Integer actions_size, Integer features_size) {
        this.memory_size = 10;
        this.memory = new CircularFifoQueue<>(memory_size);
        this.qfunction = QFunction.create(type, actions_size, features_size);
        this.type = QFunctionType.table;
        this.alfa = 0.1;
        this.gamma = 0.99;
        this.kappa = 5.0;
        this.delta_kappa = 0.01;
        this.replay_size = 3;
    }
    
    public Learner(Integer actions_size, Integer features_size, QFunctionType type, Double alfa, Double gamma, Double kappa, Double delta_kappa, Integer memory_size, Integer replay_size) {
        this.memory_size = memory_size;
        this.memory = new CircularFifoQueue<>(memory_size);
        this.qfunction = QFunction.create(type, actions_size, features_size);
        this.type = type;
        this.alfa = alfa;
        this.gamma = gamma;
        this.kappa = kappa;
        this.delta_kappa = delta_kappa;
        this.replay_size = replay_size;
    }
      
    private Double softMax(Integer index, List<Double> values) {
        Double sum = 0.0;
        for(Integer i=0; i<values.size(); i++)
            sum += (Double)Math.pow(kappa, values.get(i));  
        Double res = (Double)Math.pow(kappa, values.get(index)) / sum;
        return res;
    }
    
    private Integer weightedRandom(List<Double> items) {
        Double totalWeight = 0.0;
        for (Double i : items)
            totalWeight += i;
        Integer index = -1;
        Double random = Math.random() * totalWeight;
        for (Integer i = 0; i < items.size(); ++i) {
            random -= items.get(i);
            if (random <= 0.0d) {
                index = i;
                break;
            }
        }
        return index;
    }
            
    private Integer selectMove(Status status) {
        List<Double> probabilites = new ArrayList<>();
        List<Double> qvalues = new ArrayList<>();

        //return qfunction.best(status.getValues());
                
        for(int i=0; i<qfunction.getActionSize(); i++)
            qvalues.add(qfunction.get(status.getValues(), i));

        for(int i=0; i<qfunction.getActionSize(); i++)
            probabilites.add(softMax(i, qvalues));
            
        //for(int i=0; i<qfunction.getActionSize(); i++)
        //    if(!(status.get(i)>0 || status.get(i)<0))
        //        probabilites.add(softMax(i, qvalues));
        //    else
        //        probabilites.add(0.0);
        
        int index = weightedRandom(probabilites);

        return index;
    }
    
    public void update() {
        if(!memory.isEmpty()) {
            for(Integer i=0; i<replay_size; i++) {
                Integer index = random.nextInt(memory.size());
                Interaction interaction = memory.get(index);
                //if(interaction.getReward() >= 1.0)
                //    System.out.println("pippo");
                Double old_value = qfunction.get(interaction.getBefore().getValues(), interaction.getActionIndex());
                Double max_value = qfunction.max(interaction.getAfter().getValues());
                Double new_value = (1 - alfa) * old_value + alfa * (interaction.getReward() + gamma * max_value);
                qfunction.update(interaction.getBefore().getValues(), interaction.getActionIndex(), new_value);
            }
        }
    }
    
    public void memorize(Status new_status, Double reward) {
        if(status != null)
            memory.add(new Interaction(status, action, new_status, reward));
    }

    public int next(Status status) {
        this.status = new Status(status);
        this.action = selectMove(status); 
        return action;
    }
    
    public final void load() { 
        qfunction.load(); 
    }
    
    public void save() { 
        qfunction.save(); 
    }

    public void beginEpisode() {
    }

    public void endEpisode() {
        kappa += delta_kappa;
    }

    public void lose() {
    }

    public void win() {
    }
}
