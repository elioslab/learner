package wondertech.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Network {
    private Integer nInputs, nHidden;  
    private Double[] input, hidden;
    private Double output;
    private Double[][] weightL1;
    private Double [] weigthL2;  
    private Double learningRate = 0.1;

    public Network(Integer nInput, Integer nHidden) {
        this.nInputs = nInput;
        this.nHidden = nHidden;

        input = new Double[nInput+1];
        hidden = new Double[nHidden+1];

        weightL1 = new Double[nHidden+1][nInput+1];
        weigthL2 = new Double[nHidden+1];

        generateRandomWeights();
    }

    public Double evaluate(Double[] pattern) {
        input[0] = 1.0;
        for(Integer i=0; i<nInputs; i++)
            input[i+1] = pattern[i];
        
        hidden[0] = 1.0;
        for(Integer i=1; i<=nHidden; i++) {
            hidden[i] = 0.0;
            for(Integer j=0; j<=nInputs; j++)
                hidden[i] += weightL1[i][j] * input[j];
            hidden[i] = 1.0/(1.0 + Math.exp(-hidden[i]));
        }

        output = 0.0;
        for(Integer i=0; i<=nHidden; i++)
            output += weigthL2[i] * hidden[i];
        output = 1.0/(1.0 + Math.exp(-output));

        return output;
    }

    public void train(Double[] pattern, Double desiredOutput) {
        Double output = evaluate(pattern);
        
        //Double errorL2 = output * (1.0 - output) * (desiredOutput - output);
        Double errorL2 = desiredOutput - output;
             
        Double[] errorL1 = new Double[nHidden+1];
        for(Integer i=0; i<=nHidden; i++)
            //errorL1[i] = hidden[i] * (1.0 - hidden[i]) * weigthL2[i] * errorL2;
            errorL1[i] = hidden[i] * weigthL2[i] * errorL2;

        for(Integer i=0; i<=nHidden; i++)
            for(Integer j=0; j<=nInputs; j++) 
                weightL1[i][j] += learningRate * errorL1[i] * input[j];
        
        for(Integer i=0; i<=nHidden; i++)
            weigthL2[i] += learningRate * errorL2 * hidden[i];    
    }
    
    public final void load(Integer index) {
        InputStream file = null;
        try {
           if (Files.exists(Paths.get("network-"+index+".ser"))) {
                file = new FileInputStream("network-"+index+".ser");
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput input_file = new ObjectInputStream (buffer);
                nInputs = (Integer)input_file.readObject();
                nHidden = (Integer)input_file.readObject();
                input = (Double[])input_file.readObject();
                hidden = (Double[])input_file.readObject();
                output = (Double)input_file.readObject();
                weightL1 = (Double[][])input_file.readObject();
                weigthL2 = (Double[])input_file.readObject();

           }
        } 
        catch (Exception ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                if (Files.exists(Paths.get("network-"+index+".ser")))
                    file.close();
            } 
            catch (IOException ex) {
                Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void save(int index) {
        try {
            OutputStream file = new FileOutputStream("network-"+index+".ser");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);

            output.writeObject(this.nInputs);
            output.writeObject(this.input);
            output.writeObject(this.hidden);
            output.writeObject(this.output);
            output.writeObject(this.weightL1);
            output.writeObject(this.weigthL2);

            output.close();
        } 
        catch (IOException ex) {
            Logger.getLogger(Network.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void generateRandomWeights() {
        for(Integer j=0; j<=nHidden; j++)
            for(Integer i=0; i<=nInputs; i++) {
                weightL1[j][i] = 0.0;
        }
        
        for(Integer i=0; i<=nHidden; i++)
            weigthL2[i] = 0.0;
    }
}