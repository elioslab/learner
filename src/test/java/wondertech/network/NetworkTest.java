package wondertech.network;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import wondertech.network.Network;

public class NetworkTest {
    
    public NetworkTest() {
    }
    
    @Test
    public void test() {
        Network network = new Network(2, 3);
        
        Double[] pattern_1 = new Double[2];
        pattern_1[0] = 1.0;
        pattern_1[1] = -1.0;
        
        Double[] pattern_2 = new Double[2];
        pattern_2[0] = 1.0;
        pattern_2[1] = -0.0;
        
        for(int i=0; i<10000; i++) {
            Double value_before_1 = network.evaluate(pattern_1);
            network.train(pattern_1, 0.2);
            Double value_after_1 = network.evaluate(pattern_1);
            
            Double value_before_2 = network.evaluate(pattern_2);
            network.train(pattern_2, 0.4);
            Double value_after_2 = network.evaluate(pattern_2);
            
            System.out.println(value_before_1 + " "  + value_after_1 + " - " + value_before_2 + " "  + value_after_2);
        }
    }
}
